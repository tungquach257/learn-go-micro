package main

import (
	log "github.com/micro/go-micro/v2/logger"

	"learn-go-micro/apigw/handler"

	"github.com/micro/go-micro/v2"

	apigw "learn-go-micro/apigw/proto/apigw"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.api.apigw"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init(
	// create wrap for the Apigw service client
	// micro.WrapHandler(client.ApigwWrapper(service)),
	)

	// Register Handler
	apigw.RegisterApigwHandler(service.Server(), &handler.Apigw{Client: service.Client()})

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
