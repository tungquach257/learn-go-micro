package handler

import (
	"context"

	"github.com/google/uuid"
	"github.com/micro/go-micro/v2/client"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/metadata"

	apigw "learn-go-micro/apigw/proto/apigw"
	hello "learn-go-micro/hello/proto/hello"

	api "github.com/micro/go-micro/v2/api/proto"
)

type Apigw struct {
	Client client.Client
}

func extractValue(pair *api.Pair) string {
	if pair == nil {
		return ""
	}
	if len(pair.Values) == 0 {
		return ""
	}
	return pair.Values[0]
}

// Apigw.Call is called by the API as /apigw/call with post body {"name": "foo"}
func (e *Apigw) Call(ctx context.Context, req *apigw.Request, rsp *apigw.Response) error {
	log.Info("Received Apigw.Call request")
	log.Infof("Received Apigw.Call request: %v", ctx.Value("traceID"))

	// add a unique request id to context

	// make copy
	ctx = metadata.Set(ctx, "traceID", uuid.New().String())
	md, _ := metadata.FromContext(ctx)
	log.Infof("md: %v", md)
	helloService := hello.NewHelloService("go.micro.service.hello", e.Client)
	helloService.Call(ctx, &hello.Request{
		Name: "hello",
	})

	// // extract the client from the context
	// apigwClient, ok := client.ApigwFromContext(ctx)
	// if !ok {
	// 	return errors.InternalServerError("go.micro.api.apigw.apigw.call", "apigw client not found")
	// }

	// // make request
	// response, err := apigwClient.Call(ctx, &apigw.Request{
	// 	Name: extractValue(req.Post["name"]),
	// })
	// if err != nil {
	// 	return errors.InternalServerError("go.micro.api.apigw.apigw.call", err.Error())
	// }

	// b, _ := json.Marshal(response)

	// rsp.StatusCode = 200
	// rsp.Body = string(b)
	rsp.Msg = "Hello " + req.Name

	return nil
}
