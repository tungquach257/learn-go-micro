package client

import (
	"context"

	apigw "learn-go-micro/apigw/proto/apigw"

	"github.com/google/uuid"
	"github.com/micro/go-micro/metadata"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/server"
)

type apigwKey struct{}

// FromContext retrieves the client from the Context
func ApigwFromContext(ctx context.Context) (apigw.ApigwService, bool) {
	c, ok := ctx.Value(apigwKey{}).(apigw.ApigwService)
	return c, ok
}

// Client returns a wrapper for the ApigwClient
func ApigwWrapper(service micro.Service) server.HandlerWrapper {
	client := apigw.NewApigwService("go.micro.service.template", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, apigwKey{}, client)

			tmd := metadata.Metadata{}
			tmd["traceID"] = uuid.New().String()
			ctx = metadata.NewContext(ctx, tmd)

			return fn(ctx, req, rsp)
		}
	}
}
