module learn-go-micro

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.1
	github.com/micro/go-micro v1.18.0
	github.com/micro/go-micro/v2 v2.7.0
)
